import Vue from 'vue'
import VueRouter from 'vue-router'
// 路由整合
Vue.use(VueRouter)

// 导入对应的路由

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/discovery',
    },
    {
      // 发现音乐
      path: '/discovery',
      component: () => import('@/views/Discovery.vue'),
    },
    {
      // 推荐歌单
      path: '/playlists',
      component: () => import('@/views/Playlists.vue'),
    },
    {
      // 推荐歌单
      path: '/playlist',
      component: () => import('@/views/Playlist.vue'),
    },
    {
      // 最新音乐
      path: '/songs',
      component: () => import('@/views/Songs.vue'),
    },
    {
      // 最新音乐
      path: '/mvs',
      component: () => import('@/views/Mvs.vue'),
    },
    // mv详情
    {
      path: '/mv',
      component: () => import('@/views/Mv.vue'),
    },
    // 搜索结果页
    {
      path: '/result',
      component: () => import('@/views/Result.vue'),
    },
  ],
})

export default router
